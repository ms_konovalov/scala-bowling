Bowling scorecard
=================

The task is to make an API that allows to calculate the score of 10-pin bowling game.


Scoring rules:
--------------

##### Strike `(X)`
Strike worth 10 plus the value of next 2 rolls.
E.g.:

```
(X), (X), (X) = 30
(X), (X), (2,7) = 22
(X), (2,/) = 20
(X), (2,3) = 15
(X), (F, 2) = 12
```

#### Spare `(N,/)`
Spare worth 10 plus value of next 1 roll
E.g.:

```
(2,/), (X) = 20,...
(2,/), (5,/) = 15,...
(2,/), (2,4) = 12+6
```

#### Open frame `(2,3)`

```
(2,3) = 5
```

#### Fault `(F)`

```
(F,2) = 2
(2,F) = 2
(F,/) = 10
```

#### Miss `(-)`

```
(5,-) = 5
(-,/) = 10
```

10th round:
-----------
If you scored a strike with the first throw you have 2 more attempts.

```
(X,4,5) = 19
(X,X,5) = 25
(X,4,/) = 20
(X,X,X) = 30
```

If you scored a spare with the first throw you have 1 more attempt

```
(2,/,6) = 16
(F,/,-) = 10
```

If you scored open frame you don't have additional attempt

```
(2,3) = 5
```

How to run
==========

To understand the API see `ms.konovalov.bowling.ScoreCardTest.scala`

```scala
import ms.konovalov.bowling._
import ComplexImplicits._

val frames = Seq[Frame](X, (7,/), (7,2), (9,/), X, X, X, (2,3), (6,/), (7,/,3))
val res = CardProcessor.addFrames(CardProcessor.createEmptyCard(), frames)
```

To run tests execute `sbt test`
