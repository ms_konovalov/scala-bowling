name := "bowling-scorecard"

version := "0.1"

scalaVersion := "2.13.0"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"
