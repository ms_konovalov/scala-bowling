package ms.konovalov.bowling

import org.scalatest._
import org.scalatest.prop.TableDrivenPropertyChecks

class ScoreCardTest extends FreeSpec with TableDrivenPropertyChecks with Matchers {

  import ComplexImplicits._

  import scala.language.implicitConversions

  val cases = Table(
    ("initScore", "frames", "expectedScore", "expectedIsComplete", "expectedUnprocessed"),
    (Card(), Seq[RegFrame](X), 0, false, Vector[RegFrame](X)),
    (Card(), Seq[RegFrame]((2, 3)), 5, false, Vector[RegFrame]()),
    (Card(), Seq[RegFrame]((2, /)), 0, false, Vector[RegFrame]((2, /))),
    (Card(), Seq[RegFrame]((2, /), X), 20, false, Vector[RegFrame](X)),
    (Card(), Seq[RegFrame]((2, /), (5, /)), 15, false, Vector[RegFrame]((5, /))),
    (Card(), Seq[RegFrame]((2, /), (2, 4)), 12 + 6, false, Vector[RegFrame]()),
    (Card(), Seq[RegFrame]((F, -)), 0, false, Vector[RegFrame]()),
    (Card(), Seq[RegFrame]((F, /)), 0, false, Vector[RegFrame]((F, /))),
    (Card(), Seq[RegFrame](X, (2, /)), 20, false, Vector[RegFrame]((2, /))),
    (Card(), Seq[RegFrame](X, (2, 3)), 15 + 5, false, Vector[RegFrame]()),
    (Card(), Seq[RegFrame](X, (F, 3)), 13 + 3, false, Vector[RegFrame]()),
    (Card(), Seq[RegFrame](X, X), 0, false, Vector[RegFrame](X, X)),
    (Card(), Seq[RegFrame](X, X, (2, /)), (10 + 10 + 2) + (10 + 10), false, Vector[RegFrame]((2, /))),
    (Card(), Seq[RegFrame](X, X, X), 30, false, Vector[RegFrame](X, X)),
    (Card(), Seq[RegFrame](X, X, (2, 7)), (10 + 10 + 2) + (10 + 2 + 7) + (2 + 7), false, Vector[RegFrame]()),
  )

  "Add frames should pass" in {
    forAll(cases) { (initScore: Card, frames: Seq[RegFrame], expectedScore: Int, expectedIsComplete: Boolean, expectedUnprocessed: Vector[RegFrame]) =>
      val res = CardProcessor.addFrames(initScore, frames)
      res.score should be(expectedScore)
      res.isComplete should be(expectedIsComplete)
      res.unprocessed should be(expectedUnprocessed)
    }
  }

  val invalidCases = Table(
    "frames",
    (-1, 5),
    (0, -5),
    (11, 15),
    (1, 15),
  )

  "Invalid cases should fail" in {
    forAll(invalidCases) { input: (Int, Int) =>
      an[IllegalArgumentException] should be thrownBy {
        val frame: RegFrame = input
        CardProcessor.addFrame(Card(), frame)
      }
    }
  }

  val cases10 = Table(
    ("initScore", "frame", "expectedScore", "expectedIsComplete", "expectedUnprocessed"),
    (Card(frameNum = 9), Open(2, 5), 7, true, Vector[RegFrame]()),
    (Card(frameNum = 9, score = 100), Open(2, 5), 107, true, Vector[RegFrame]()),
    (Card(frameNum = 9), Spare10(2, 3), 13, true, Vector[RegFrame]()),
    (Card(frameNum = 9), Spare10(2, X), 20, true, Vector[RegFrame]()),
    (Card(frameNum = 9), XXX, 30, true, Vector[RegFrame]()),
  )

  "Add frame10 should pass" in {
    forAll(cases10) { (initScore: Card, frame: Frame10, expectedScore: Int, expectedIsComplete: Boolean, expectedUnprocessed: Vector[RegFrame]) =>
      val res = CardProcessor.addFrame(initScore, frame)
      res.score should be(expectedScore)
      res.isComplete should be(expectedIsComplete)
      res.unprocessed should be(expectedUnprocessed)
    }
  }

  val invalidCases10 = Table(
    ("initScore", "frame"),
    (Card(frameNum = 8, score = 100), XXX),
    (Card(frameNum = 10, isComplete = true), Spare10(2, 3)),
    (Card(frameNum = 2), Spare10(2, X)),
    (Card(frameNum = 8), XXX),
  )

  "Add frame10 should fail" in {
    forAll(invalidCases10) { (initScore: Card, frame: Frame10) =>
      an[IllegalArgumentException] should be thrownBy {
        CardProcessor.addFrame(initScore, frame)
      }
    }
  }

  val fullCases = Table(
    ("frames", "expectedScore", "expectedIsComplete", "expectedUnprocessed"),
    (Seq[Frame](X, X, X, X, X, X, X, X, X, XXX), 300, true, Vector[RegFrame]()),
    (Seq[Frame]((2,1), (2,1), (2,1), (2,1), (2,1), (2,1), (2,1), (2,1), (2,1), Open(2,1)), 30, true, Vector[RegFrame]()),
    (Seq[Frame](X, X, (-,-), (-,-), (-,-), (-,-), (-,-), (-,-), (-,-), XXX), 60, true, Vector[RegFrame]()),
    (Seq[Frame](X, (7,/), (7,2), (9,/), X, X, X, (2,3), (6,/), Spare10(7,3)), 168, true, Vector[RegFrame]()),
  )

  "Full cases should pass" in {
    forAll(fullCases) { (frames: Seq[Frame], expectedScore: Int, expectedIsComplete: Boolean, expectedUnprocessed: Vector[RegFrame]) =>
      val res = CardProcessor.addFrames(CardProcessor.createEmptyCard(), frames)
      res.score should be(expectedScore)
      res.isComplete should be(expectedIsComplete)
      res.unprocessed should be(expectedUnprocessed)
    }
  }
}


