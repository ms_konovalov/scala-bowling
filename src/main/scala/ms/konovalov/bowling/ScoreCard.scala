package ms.konovalov.bowling

import ComplexImplicits._
import scala.language.implicitConversions

/*
 * Here comes the data model describing the bowling game
 * The aim is to make it as type-safe as possible
 * The bowling game consists of 10 Frames: 9 simple frames and 10th advanced frame
 * Frames from 1 to 9 consist of 2 attempts
 */

/**
  * Trait represents first attempt in frame 1 to 9
  */
sealed trait FirstAttempt {
  def score: Int
}

/**
  * Trait represents second attempt in frame 1 to 9
  */
sealed trait SecondAttempt {
  def score: Int
}

/**
  * Special representation of the second attempt that ends with spare
  */
case object /

/**
  * Fault may happen in both first of second attempt
  */
case object F extends FirstAttempt with SecondAttempt with FillAttempt {
  def score = 0
}

/**
  * Miss can happen in both first of second attempt
  */
case object - extends FirstAttempt with SecondAttempt with FillAttempt {
  def score = 0
}

/**
  * Regular attempt
  *
  * @param score amount of scored points
  */
case class NumAttempt(score: Int) extends FirstAttempt with SecondAttempt with FillAttempt {
  require(score > 0, "must be > 0")
  require(score < 10, "must be < 10")
}

/**
  * Common interface for frames
  */
sealed trait Frame {
  def score: Int
}

/**
  * Trait represents frame
  */
sealed trait RegFrame extends Frame {
  def score: Int
}

/**
  * Strike happens when all pins fall with first attempt
  */
case object X extends RegFrame with FillAttempt {
  def score = 10
}

/**
  * Frame with Spare happens when all pins fall with second attempt
  *
  * @param first amount of points scored with the first attempt
  */
case class /(first: FirstAttempt) extends RegFrame {
  def score = 10
}

/**
  * Open frame happens when not all pins fall
  *
  * @param first  amount of points scored with the first attempt
  * @param second amount of points scored with the second attempt
  */
case class Open(first: FirstAttempt, second: SecondAttempt) extends RegFrame with Frame10 {
  require(score <= 10, "must be <= 10")
  require(score >= 0, "must be >= 0")

  def score: Int = first.score + second.score
}

/**
  * Trait for frame 10: in frame 10 if you get Strike with the first attempt you get 2 more attempts
  * If you get Spare you get 1 additional attempt
  */
sealed trait Frame10 extends Frame

/**
  * Frame 10 with 3 strikes in a row
  */
case object XXX extends Frame10 {
  def score = 30
}

/**
  * Additional fill attempt for frame 10
  */
sealed trait FillAttempt {
  def score: Int
}

/**
  * Spare in round 10
  *
  * @param first amount of points scored with the first attempt
  * @param fill  amount of points scored with the fill attempt
  */
case class Spare10(first: FirstAttempt, fill: FillAttempt) extends Frame10 {
  def score: Int = 10 + fill.score
}

/**
  * Syntactic sugar for data model
  * (unfortunately not fully available in Table driven tests)
  */
object ComplexImplicits {
  implicit def int2Attempt(x: Int): NumAttempt = NumAttempt(x)

  implicit def tuple2Frame1(x: (FirstAttempt, SecondAttempt)): Open = Open(x._1, x._2)

  implicit def tuple2Frame2(x: (Int, Int)): Open = Open(x._1, x._2)

  implicit def tuple2Frame3(x: (Int, SecondAttempt)): Open = Open(x._1, x._2)

  implicit def tuple2Frame4(x: (FirstAttempt, Int)): Open = Open(x._1, x._2)

  implicit def tuple2Frame5(x: (FirstAttempt, /.type)): / = /(x._1)

  implicit def tuple2Frame6(x: (Int, /.type)): / = /(x._1)

  implicit def tuple2Frame7(x: (Int, /.type, X.type)): Spare10 = Spare10(x._1, x._3)

  implicit def tuple2Frame8(x: (Int, /.type, Int)): Spare10 = Spare10(x._1, x._3)
}

/**
  * Class representing Score card for single user
  *
  * @param unprocessed collection of frames that cannot be processed at the moment as the score depends on the next frame result
  * @param score       current score
  * @param frameNum    number of the passed frames
  * @param isComplete  whether game is finished or not
  */
case class Card(unprocessed: Vector[RegFrame] = Vector(), score: Int = 0, frameNum: Int = 0, isComplete: Boolean = false)

/**
  * Processor class, magic is implemented here
  */
object CardProcessor {

  /**
    * Create empty score card
    *
    * @return empty score card
    */
  def createEmptyCard(): Card = Card()

  /**
    * Add frame and calculate the score
    *
    * @param card  current score card
    * @param frame result of frame
    * @return new instance of score card with result of frame
    * @throws IllegalStateException if the game is finished already or the frame 10 is sent when regular
    *                               frame was expected
    */
  def addFrame(card: Card, frame: Frame): Card = {
    if (card.isComplete || card.frameNum >= 10) {
      throw new IllegalArgumentException("Game is finished")
    }
    frame match {
      case x: Open => if (card.frameNum < 9) addRegFrame(card, x) else addFrame10(card, x)
      case x: RegFrame => addRegFrame(card, x)
      case x: Frame10 =>
        if (card.frameNum < 9) throw new IllegalArgumentException("Regular frame is needed") else addFrame10(card, x)
    }
  }

  /**
    * Add frame 10 and calculate the score
    * The frame 10 is transformed into a list of regular frames
    * [[XXX]] -> [X, X, X] (3 strikes)
    * [[Spare10]] -> [spare, fill], where fill -> Strike or fill -> [[RegFrame]] of miss and
    * [[Open]] is not transformed
    *
    * @param card  current score card
    * @param frame result of frame 10
    * @return new instance of score card with result of frame 10
    */
  private def addFrame10(card: Card, frame: Frame10): Card = {
    val seq = frame match {
      case x@Open(_, _) => Seq[RegFrame](x)
      case XXX => Seq(X, X, X)
      case Spare10(first, fill) => fill match {
        case X => Seq[RegFrame]((first.score, /), X)
        case _ => Seq[RegFrame]((first.score, /), (-, fill.score))
      }
    }
    val res = addFrames(card, seq, addRegFrame)
    Card(score = res.score, frameNum = 10, isComplete = true)
  }

  /**
    * Add frame 1 to 9 and calculate current score
    * Calculation logic depends on the lost of unprocessed frames and the current frame
    *
    * @param card  score card with current result
    * @param frame frame to process
    * @return new instance of the score card
    */
  private def addRegFrame(card: Card, frame: RegFrame): Card = {
    val (unprocessed, score) = card.unprocessed match {
      case Vector(X, X) =>
        frame match {
          case X => (
            Vector[RegFrame](X) ++ toUnprocessed(frame),
            X.score + X.score + getFirstScore(frame) + toAddScore(frame),
          )
          case _ => (
            Vector[RegFrame]() ++ toUnprocessed(frame),
            (X.score + X.score + getFirstScore(frame)) + (X.score + frame.score) + toAddScore(frame),
          )
        }
      case Vector(X) =>
        frame match {
          case X => (
            Vector[RegFrame](X) ++ toUnprocessed(frame),
            0 + toAddScore(frame),
          )
          case _ => (
            Vector[RegFrame]() ++ toUnprocessed(frame),
            X.score + frame.score + toAddScore(frame),
          )
        }
      case Vector(x@ /(_)) => (
        Vector[RegFrame]() ++ toUnprocessed(frame),
        x.score + getFirstScore(frame) + toAddScore(frame),
      )
      case Vector() => (
        Vector[RegFrame]() ++ toUnprocessed(frame),
        toAddScore(frame),
      )
    }
    Card(unprocessed, card.score + score, card.frameNum + 1)
  }

  /*
   * Determines if the score may be calculated at the moment
   */
  private def toAddScore(frame: RegFrame): Int = frame match {
    case Open(x, y) => x.score + y.score
    case _ => 0
  }

  /*
   * Determines if the frame depends on the following frame or not
   */
  private def toUnprocessed(frame: RegFrame): Vector[RegFrame] = frame match {
    case X | /(_) => Vector(frame)
    case _ => Vector()
  }

  /*
   * Takes score of the first attempt of the frame
   */
  private def getFirstScore(frame: RegFrame): Int = frame match {
    case X => X.score
    case /(x) => x.score
    case Open(x, _) => x.score
  }

  /*
   * Typed method to process list of frames
   */
  private def addFrames[T](card: Card, frames: Seq[T], fun: (Card, T) => Card): Card =
    frames.foldLeft(card)((card, frame) => fun(card, frame))

  /**
    * Add collection of frames
    *
    * @param card   initial score card
    * @param frames frames to process
    * @return new score card with calculated data
    */
  def addFrames(card: Card, frames: Seq[Frame]): Card = addFrames(card, frames, addFrame)
}
